﻿namespace DotStat.Transfer.Excel.Excel
{
    public struct MatchPattern
    {
        public readonly string Source;
        public readonly string Pattern;
        public readonly bool IsRegex;
        public readonly bool IsExpression;
        public readonly bool IsCondition;

        public MatchPattern(
            string pattern,
            bool isRegex,
            bool isExpression = false,
            bool isCondition = false,
            string source = null)
        {
            Pattern = pattern;
            IsRegex = isRegex;
            IsExpression = isExpression;
            IsCondition = isCondition;
            Source = source;
        }
    }
}
