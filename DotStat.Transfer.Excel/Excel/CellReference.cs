using System.Drawing;
using System.Text.RegularExpressions;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Util;

namespace DotStat.Transfer.Excel.Excel
{
    public class CellReference : ExpresssionBase
    {
        public static readonly Regex CellReferenceRegex = new Regex(@"^([^!]+!)?([A-Z]+)([1-9]+[0-9]*)$",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public static string ExtractCellCoordinate(string coordinates)
        {
            if (string.IsNullOrWhiteSpace(coordinates))
            {
                return null;
            }
            var match = CellReferenceRegex.Match(coordinates);

            if (!match.Success)
            {
                throw new ApplicationArgumentException(
                    "The cell reference {0} is not a valid excell address".F(coordinates));
            }

            return match.Groups[2].Value + match.Groups[3].Value;
        }

        public static string ExtractSheetName(string coordinates)
        {
            if (string.IsNullOrWhiteSpace(coordinates))
            {
                return null;
            }
            var match = CellReferenceRegex.Match(coordinates);

            if (!match.Success)
            {
                throw new ApplicationArgumentException(
                    "The cell reference {0} is not a valid excell address".F(coordinates));
            }

            var res = match.Groups[1].Value;
            return string.IsNullOrWhiteSpace(res) ? null : res.Substring(0, res.Length - 1);
        }

        public static Point ConvertCoordinate(string coordinates)
        {
            var match = CellReferenceRegex.Match(coordinates);

            if (!match.Success)
            {
                throw new ApplicationArgumentException(
                    "The cell reference {0} is not a valid excell address".F(coordinates));
            }

            var colletters = match.Groups[2].Value.ToUpperInvariant();
            var row = int.Parse(match.Groups[3].Value);
            int col = 0;
            int mult = 1;

            for (int ii = colletters.Length - 1; ii >= 0; ii--)
            {
                col = col + (colletters[ii] - 64)*mult;
                mult *= 26;
            }

            return new Point(col, row);
        }

        public static string ConvertCoordinate(int row, int column, string sheetName)
        {
            var res = "";

            if (!string.IsNullOrWhiteSpace(sheetName))
            {
                res = sheetName + '!';
            }

            var colstr = "";

            do
            {
                colstr = ((char)('A' + ((column - 1)%26))) + colstr;
                column = (column - ((column - 1)%26))/26;
            } while (column > 0);

            res += colstr + row;
            return res;
        }

        public static readonly CellReference Null = new CellReference();

        private readonly int _Row;
        private readonly int _Column;
        private readonly string _Coordinates;


        // to create null literal
        private CellReference()
            : base(null, null)
        {
            _Row = 0;
            _Column = 0;
        }

        public CellReference(int row, int column, string sheetName = null)
            : base(sheetName, null)
        {
            _Row = row;
            _Column = column;
            _Coordinates = ConvertCoordinate(row, column, sheetName);
            EvaluationSource = _Coordinates;
        }

        public CellReference(string coordinates)
            : base(ExtractSheetName(coordinates), null)
        {
            if (!CellReferenceRegex.IsMatch(coordinates))
            {
                throw new ApplicationArgumentException(
                    ":-The cell reference {0} is not a valid excell address\n".F(coordinates));
            }

            var pt = ConvertCoordinate(coordinates);
            _Row = pt.Y;
            _Column = pt.X;
            _Coordinates = ExtractCellCoordinate(coordinates);
            EvaluationSource = coordinates;
        }

        public override int Row
        {
            get { return _Row; }
        }

        public override int Column
        {
            get { return _Column; }
        }

        public override string Coordinates
        {
            get { return _Coordinates; }
        }

        public static string DetermineSheetName(ref string cellAddress, string sheetName)
        {
            if (string.IsNullOrWhiteSpace(cellAddress))
            {
                return sheetName;
            }
            var adrSheet = ExtractSheetName(cellAddress);
            if (adrSheet != null && sheetName != null && !adrSheet.IsSameAs(sheetName))
            {
                throw new ApplicationArgumentException(
                    "The cell address {0} and the given sheet name {1} does not match".F(adrSheet,
                        sheetName));
            }
            var sheet = sheetName ?? adrSheet;
            cellAddress = ExtractCellCoordinate(cellAddress);
            return sheet;
        }
    }
}