﻿using System;
using System.Collections;
using DotStat.Domain;
using DotStat.Transfer.Excel.Mapping;

namespace DotStat.Transfer.Excel.Reader
{
    /// <summary>
    ///     This is the base class for all ISWReader implementations.
    /// </summary>
    public abstract class SWReaderBase<T> : ISWReader<T> where T:class
    {
        private readonly Dataflow _Dataset;
        private T _Current;

        protected SWReaderBase(Dataflow ds)
        {
            _Dataset = ds;
        }

        public abstract V8Mapper<T> Mapper { get; }

        public Dataflow Dataset
        {
            get { return _Dataset; }
        }

        public virtual void Reset()
        {
            throw new InvalidOperationException("This reader does not support resetting");
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public T Current
        {
            get { return _Current; }
        }

        object IEnumerator.Current
        {
            get { return Current; }
        }

        public bool MoveNext()
        {
            _Current = FetchAndBuildCurrent();

            return _Current != null;
        }


        /// <summary>
        ///     A sequential unique number to identify the current record, skipped records (due to errors?) also get a number
        /// </summary>
        public abstract int CurrentRecordNo { get; }

        public abstract string Name { get; }

        protected abstract int GetColumnIndex(string columnName);

        protected virtual void Dispose(bool disposing)
        {
        }

        ~SWReaderBase()
        {
            Dispose(false);
        }


        /// <summary>
        ///     This method attempts to fetch the next record from the data source and, if successfull,
        ///     populates the <ref>Current</ref> record
        /// </summary>
        protected virtual T FetchAndBuildCurrent()
        {
            if (!FetchNext())
                return null;

            var source = new string[Mapper.SourceCount];

            for (var ii = 0; ii < Mapper.SourceCount; ii++)
            {
                source[ii] = FetchField(Mapper.SourceNames[ii]);
            }

            return Mapper.Transform(source);
        }

        /// <summary>
        ///     Advances the record pointer of the data source to the next record and prepares for reading of values
        ///     This method must catch all exceptions and raise only the following if further reading can be attempted:
        ///     - a <ref>TransferInterruptedException</ref> if the user interrupts the transfer
        ///     - a <ref>ReadErrorException</ref> if an I/O error occurs during getting the data
        ///     - a <ref>IOException</ref> for all other unexpected cases
        ///     If further reading cannot be attempted, it must raise an exception appropriate for the data source
        /// </summary>
        protected abstract bool FetchNext();

        /// <summary>
        ///     Gets the external dimension member from the extern source in its original format
        /// </summary>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public abstract string FetchField(string fieldName);

        /// <summary>
        ///     The exact source of the currrent data point in its external source format
        /// </summary>
        /// <returns></returns>
        protected abstract string GetSource();

        protected virtual string GetProblemSource()
        {
            return null;
        }
    }
}