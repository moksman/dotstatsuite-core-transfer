﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Mapping;

namespace DotStat.Transfer.Excel.Reader
{
    /// <summary>
    /// this will chain record iterators of the identical type into a single record iterator
    /// </summary>
    public class IteratorChain<T,R> : IRecordIterator<R>    
        where T : class, IRecordIterator<R>
        where R: class
    {
        private readonly ICollection<T> _Iterators;
        private int _CurrentRecNo;
        private IEnumerator<T> _Enumerator;
        private bool _Exhausted;
        private T _Current;

        public IteratorChain(IEnumerable<T> iterators)
        {
            _Iterators = iterators.ToList();
            Reset();
        }

        public V8Mapper<R> Mapper => CurrentIterator.Mapper;
        public int CurrentRecordNo => _CurrentRecNo;

        public int FieldCount => CurrentIterator.FieldCount;

        public string GetField(int pos)
        {
            return CurrentIterator.GetField(pos);
        }

        public string GetFieldName(int pos)
        {
            return CurrentIterator.GetFieldName(pos);
        }

        public int GetFieldIndex(string field)
        {
            return CurrentIterator.GetFieldIndex(field);
        }

        public bool Read()
        {
            while (!_Exhausted && !_Enumerator.Current.Read())
            {
                if (!_Enumerator.MoveNext())
                {
                    _Current = null;
                    _Exhausted = true;
                    return false;
                }
                _Current = _Enumerator.Current;
                _Current?.Reset();
            }
            if (_Exhausted)
            {
                return false;
            }
            _CurrentRecNo++;
            return true;
        }

        public void Dispose()
        {
            foreach (var it in _Iterators)
            {
                it.Dispose();
            }
        }

        public void Reset()
        {
            _Exhausted = _Iterators.Count == 0;
            _Enumerator = _Iterators.GetEnumerator();
            _Enumerator.MoveNext();


            _Current = !_Exhausted
                ? _Enumerator.Current
                : null;
            _CurrentRecNo = 0;
            _Current?.Reset();
        }

        public T CurrentIterator
        {
            get
            {
                if (_Exhausted)
                {
                    throw new InvalidOperationException();
                }
                if (_Current == null)
                {
                    throw new SWApplicationException("There is no data found to iterate over");
                }
                return _Current;
            }
        }
    }
}