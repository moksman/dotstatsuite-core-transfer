﻿using System;
using System.Collections.Generic;
using System.Linq;
using DotStat.Domain;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Util;

namespace DotStat.Transfer.Excel.Mapping
{
    public abstract class SWMapper : Mapper
    {
        private struct FallbackMapping
        {
            public readonly string FallbackTarget;

            public FallbackMapping(string fallbackTarget) : this()
            {
                FallbackTarget = fallbackTarget;
            }
        }

        public string ValueFieldName
        {
            get { return _ValueFieldName; }
            set
            {
                var idx = SourceNames.IndexOf(value, StringComparer.InvariantCultureIgnoreCase);
                if (idx < 0)
                {
                    throw new ApplicationArgumentException("The field {0} does not exist in the source dataset".F(value));
                }
                SourceValueIndex = idx;
                _ValueFieldName = value;
            }
        }

        private string _ValueFieldName;
        protected internal int SourceValueIndex { get; private set; }

        public readonly int DecimalPlaces;
        public double PowerFactor { get; private set; }
        public readonly string SourceDataset;
        public readonly Dataflow TargetDataset;

        // do this mapper do any transformations?
        public bool IsTransforming { get; protected set; }

        private bool _RetainTextFormat;

        public bool RetainTextFormat
        {
            get { return _RetainTextFormat; }
            set
            {
                _RetainTextFormat = value;
                IsTransforming = IsTransforming || !value;
            }
        }

        public bool TransformMultipleChoiceQualitativeValuesToCodes { get; set; }

        private readonly string _NumberFormat;
        private Dictionary<string, Code> _DimensionMemberLookup;
        private Dictionary<string, Code> _DimensionMemberLookup2;
        private Dictionary<string, Code> _DimensionMemberLookup3;

        private readonly Dictionary<int, FallbackMapping> _FallbackMappings;
        protected internal readonly int TargetValueIndex;
        protected internal readonly int TargetCCIndex;

        protected SWMapper(Dataflow targetDataset,
            double powerFactor = 1.0,
            int decimalPlaces = 3,
            string externDataset = "",
            IEnumerable<string> sourceNames = null,
            IEnumerable<NameMatching> matchingColumns = null,
            string[] targetTemplate = null,
            string valueColumnName = null,
            string controlCodeColumnName = null)
            : base(
                BuildTuple(sourceNames, targetDataset, valueColumnName, controlCodeColumnName),
                targetDataset.GetColumnNames().ToArray(),
                matchingColumns,
                targetTemplate)
        {
            TargetDataset = targetDataset;
            DecimalPlaces = decimalPlaces;
            //this is for mapping all unrecognized members for a particular dimension to a fixed member
            _FallbackMappings = new Dictionary<int, FallbackMapping>();
            SourceDataset = externDataset ?? string.Empty;
            _NumberFormat = decimalPlaces <= 0 ? "{0:0}" : ("{0:0.".PadRight(decimalPlaces + 5, '0') + "}");

            ValueFieldName = valueColumnName ?? targetDataset.ValueFieldName;

            QualitativeSeparator = ';';

            // ReSharper disable once DoNotCallOverridableMethodsInConstructor
            UpdatePowerFactor(powerFactor);

            ////assign here to not cause exception in the setter
            //_CCFieldName = controlCodeColumnName ?? targetDataset.CCFieldName;
            //SourceCCIndex = string.IsNullOrEmpty( _CCFieldName )
            //                        ? -1
            //                        : SourceNames.IndexOf( CCFieldName, StringComparer.InvariantCultureIgnoreCase );
            //if( SourceCCIndex < 0 ) {
            //    _CCFieldName = null;
            //}

            //TargetValueIndex = TargetNames.IndexOf( TargetDataset.ValueFieldName );
            //TargetCCIndex = TargetNames.IndexOf( TargetDataset.CCFieldName );

            //AddNameMatching( ValueFieldName, targetDataset.ValueFieldName );
            //if( SourceCCIndex >= 0 && TargetCCIndex >= 0 ) {
            //    AddNameMatching( _CCFieldName, TargetDataset.CCFieldName );
            //}
        }

        public virtual void UpdatePowerFactor(double pf)
        {
            // ReSharper disable CompareOfFloatsByEqualityOperator
            if (pf == 0.0)
            {
                if (PowerFactor == 0.0)
                {
                    PowerFactor = 1.0;
                }
                return;
            }
            // ReSharper restore CompareOfFloatsByEqualityOperator
            PowerFactor = pf;
        }

        internal bool IsTextFormatSignificant
        {
            get
            {
                return false;

                //return RetainTextFormat && TargetDataset.WithQualitativeData
                //       && TargetDataset.QualitativeDimension.Members.Any( m => m.ValueType == SWValueType.Text );
            }
        }

        public override void AddMapping(string[] source, string[] target, bool isRegex)
        {
            var src = AdjustSource(source.ToArray());
            var proj = AdjustProjection(target.ToArray());

            if (!isRegex || IsAdvancedCoordMapping(source, target))
            {
                base.AddMapping(src, proj, false);
                return;
            }
            var idx = Array.IndexOf(source, ".");
            if (idx >= 0)
            {
                var tidx = target.IndexOf(t => t != null);
                if (tidx >= 0)
                {
                    //this is a fallback mapping, not part of standard mapping mechanism
                    _FallbackMappings.Add(tidx, new FallbackMapping(target[tidx]));
                }
            }
            else
            {
                base.AddMapping(src, proj, true);
            }
        }

        //two methods below trims spaces in the mappings except for value and cc mappings
        //for which empty space means mappping to null
        private string[] AdjustProjection(string[] projection)
        {
            for (var ii = 0; ii < projection.Length; ii++)
            {
                var val = projection[ii];
                if (val == null || ii == TargetValueIndex || ii == TargetCCIndex)
                {
                    continue;
                }
                if (string.IsNullOrWhiteSpace(val))
                {
                    projection[ii] = null;
                }
                else
                {
                    projection[ii] = val.Trim();
                }
            }
            return projection;
        }

        private string[] AdjustSource(string[] source)
        {
            throw new NotImplementedException();

            //for ( var ii = 0; ii < source.Length; ii++ ) {
            //    var val = source[ ii ];
            //    if( val == null || ii == SourceValueIndex || ii == SourceCCIndex ) {
            //        continue;
            //    }
            //    if( string.IsNullOrWhiteSpace( val ) ) {
            //        source[ ii ] = null;
            //    } else {
            //        source[ ii ] = val.Trim( );
            //    }
            //}
            //return source;
        }

        private bool IsValueMapping(string[] source, string[] projection)
        {
            throw new NotImplementedException();

            //var cnt = 0;
            //if( source[ SourceValueIndex ] != null ) {
            //    cnt++;
            //}
            //if( projection[ TargetValueIndex ] != null ) {
            //    cnt++;
            //}
            //if( SourceCCIndex >= 0 && source[ SourceCCIndex ] != null ) {
            //    cnt++;
            //}
            //// Todo: check with Kemal, but it seems this should be != null
            //if( TargetCCIndex >= 0 && projection[ TargetCCIndex ] == null ) {
            //    cnt++;
            //}
            //if( cnt == 0 ) {
            //    return false;
            //}

            //return source.Where( ( t, ii ) => ii != SourceValueIndex && ii != SourceCCIndex ).All( t => t == null )
            //       && projection.Where( ( t, ii ) => ii != TargetValueIndex && ii != TargetCCIndex ).All( t => t == null );
        }


        private bool IsAdvancedCoordMapping(string[] source, string[] projection)
        {
            if (!IsValueMapping(source, projection) &&
                (source.Count(v => v != null) > 1 || projection.Count(v => v != null) > 1))
            {
                return true;
            }
            for (var ii = 0; ii < SourceCount; ii++)
            {
                var src = source[ii];
                if (src != null && !HasNameMatchingForSource(ii))
                {
                    return true;
                }
            }
            for (var ii = 0; ii < TargetCount; ii++)
            {
                var prj = projection[ii];
                if (prj != null && !HasNameMatchingForTarget(ii))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Indicates if the mapping is an advanced mapping according to what the UI is able to display
        /// </summary>
        /// <param name="source"></param>
        /// <param name="projection"></param>
        /// <returns></returns>
        private bool IsAdvancedCoordMappingUi(string[] source, string[] projection)
        {
            throw new NotImplementedException();

            //if (!IsValueMappingUi(source, projection) &&
            //    (source.Count(v => v != null) > 1 || projection.Count(v => v != null) > 1 ||
            //     HasBlankCcMapping(source, projection)))
            //{
            //    return true;
            //}
            //for (var ii = 0; ii < SourceCount; ii++)
            //{
            //    var src = source[ii];
            //    if (src != null && !HasNameMatchingForSource(ii))
            //    {
            //        return true;
            //    }
            //}
            //for (var ii = 0; ii < TargetCount; ii++)
            //{
            //    var prj = projection[ii];
            //    if (prj != null && !HasNameMatchingForTarget(ii))
            //    {
            //        return true;
            //    }
            //}
            //return false;
        }

        public IEnumerable<MappingElement> AdvancedCoordMappings
        {
            get
            {
                EnsureMappingsAreBuilt();
                return MappingItems.Where(mi => IsAdvancedCoordMapping(mi.KeySource, mi.Projection));
            }
        }

        public IEnumerable<MappingElement> ValueMappings
        {
            get
            {
                return TransformToValueMappings(MappingItems.Where(mi => IsValueMapping(mi.KeySource, mi.Projection)));
            }
        }

        private IEnumerable<MappingElement> TransformToValueMappings(IEnumerable<MappingElement> mappingItems)
        {
            throw new NotImplementedException();

            //foreach (var mi in mappingItems)
            //{
            //    var src = new string[2];
            //    var prj = new string[2];
            //    src[0] = mi.KeySource[SourceValueIndex];
            //    prj[0] = mi.Projection[TargetValueIndex];
            //    if (SourceCCIndex >= 0)
            //    {
            //        src[1] = mi.KeySource[SourceCCIndex];
            //    }
            //    if (TargetCCIndex >= 0)
            //    {
            //        prj[1] = mi.Projection[TargetCCIndex];
            //    }
            //    yield return new MappingElement(-1, src, prj, mi.IsRegEx);
            //}
        }

        private static string BuildKey(string dim, string member)
        {
            return dim.Trim() + "\0" + member.Trim();
        }

        private static string[] BuildTuple(IEnumerable<string> cols, Dataflow ds, string valField, string ccField)
        {
            if (cols == null)
            {
                var dscols = ds.GetColumnNames().ToList();
                if (!string.IsNullOrEmpty(valField))
                {
                    var vidx = dscols.IndexOf(ds.ValueFieldName);
                    if (vidx < 0)
                    {
                        throw new ArgumentException(
                            "Dataflow {0}: value field name {1} is not among its columns".F(ds.Code,
                                ds.ValueFieldName));
                    }
                    dscols[vidx] = valField;
                }

                return dscols.ToArray();
            }
            var res = new IndexedSet<string>(StringComparer.InvariantCultureIgnoreCase);
            res.AddAll(cols);
            return res.ToArray();
        }

        private void EnsureMemberLookupInitialized()
        {
            throw new NotImplementedException();

            //if( _DimensionMemberLookup != null ) {
            //    return;
            //}
            //_DimensionMemberLookup = new Dictionary< string, DimensionMember >( StringComparer.InvariantCultureIgnoreCase );
            //_DimensionMemberLookup2 = new Dictionary< string, DimensionMember >( StringComparer.InvariantCultureIgnoreCase );
            //_DimensionMemberLookup3 = new Dictionary<string, DimensionMember>(StringComparer.InvariantCultureIgnoreCase);

            //foreach( var dim in TargetDataset.Dimensions ) {
            //    foreach( var mem in dim.Members ) {
            //        _DimensionMemberLookup[ BuildKey( dim.Code, mem.Id.ToString( ) ) ] = mem;
            //        _DimensionMemberLookup[ BuildKey( dim.Code, mem.Code ) ] = mem;
            //        _DimensionMemberLookup3[ BuildKey( dim.Code, mem.EnglishName ) ] = mem;
            //        _DimensionMemberLookup3[ BuildKey( dim.Code, mem.FrenchName ) ] = mem;
            //    }

            //    foreach( var mapping in Database.Instance.GetDefaultMemberMappings( dim.SWDimensionRefId ) ) {
            //        var mem = dim.FindMember( mapping.Value );
            //        if( mem != null ) {
            //            _DimensionMemberLookup2[ BuildKey( dim.Code, mapping.Key ) ] = mem;
            //        }
            //    }
            //}
        }

        /// <summary>
        /// Takes as parameter array of where with fixed indexes. First is primary value, then dimension codes (in the same order as Dataflow.Dimension collection)
        /// then Observation attributes (in the same order as Dataflow.Attributes)
        /// </summary>
        /// <param name="source">plain array: value, dimension codes, obs attribute values</param>
        /// <returns></returns>
        public virtual IOResult Transform(string[] source)
        {
            throw new NotImplementedException();
        }

        protected virtual IOResult TransformDimensions(string[] target, Observation src)
        {
            throw new NotImplementedException();

            /*
            var status = IOStatus.OK;
            var errors = new StringBuilder( );
            var vt = src.ValueType;

            for( var ii = 0; ii < TargetCount; ii++ ) {
                if( ii == TargetValueIndex || ii == TargetCCIndex ) {
                    continue;
                }
                var dim = TargetNames[ ii ];
                var mv = target[ ii ];
                if( string.IsNullOrWhiteSpace( mv ) ) {
                    status |= IOStatus.MissiongMemberError;
                    if( errors.Length > 0 ) {
                        errors.Append( '\n' );
                    }
                    errors.AppendFormat( "The data source does not have a value for dimension {0}", TargetNames[ ii ] );
                    continue;
                }
                var mem = string.IsNullOrEmpty( mv ) ? null : GetSWDimensionMember( dim, mv );
                FallbackMapping fallback;
                if( mem == null && _FallbackMappings.TryGetValue( ii, out fallback ) ) {
                    mv = fallback.FallbackTarget;
                    target[ ii ] = fallback.FallbackTarget;
                    //a fallback member is used here
                    mem = GetSWDimensionMember( dim, fallback.FallbackTarget );
                }

                if( mem == null )
                {
                    // When exporting to .Stat, we signal errors on the coordinates when the value is not null
                    // Note 1: This has been restricted to export to .Stat by naming .Stat mappers, but this may be a problem for
                    //         other exports as well.
                    // Note 2: If this stays limited to .Stat, maybe this could be done in an override of CanWriteRecord in 
                    //         DotStatDatFileWriter?
                    if (Name.EndsWith("Reversed-DotStatMapper"))
                    {
                        status |= IOStatus.UnrecognizedMemberError;
                        if (errors.Length > 0)
                        {
                            errors.Append('\n');
                        }
                        var nbValueCc = 1; // SourceValueIndex is always a positive number
                        if (SourceCCIndex >= 0)
                        {
                            nbValueCc++;
                        }
                        var arrCoords = new string[SourceCount - nbValueCc];
                        var iCoord = 0;
                        for (var iSrc = 0; iSrc < SourceCount; iSrc++)
                        {
                            if (iSrc == SourceValueIndex || iSrc == SourceCCIndex) continue;
                            arrCoords[iCoord] = src.Record[iSrc];
                            iCoord++;
                        }
                        errors.AppendFormat("Unable to find a correspondance for the coordinates ({0})",
                            string.Join(", ", arrCoords));
                        break;
                    }
                }
                if( mem != null && mem.ValueType != SWValueType.Value ) {
                    vt = mem.ValueType;
                }
            }
            return new IOResult( status, errors.ToString( ), new Observation( target, vt, src.Source ), sourceData: src.Record );

            */
        }

        protected virtual IOStatus TransformControlCode(string cc, out string ccVal)
        {
            throw new NotImplementedException();

            //if( string.IsNullOrWhiteSpace( cc ) || TargetDataset.ControlCodes.Count == 0 ) {
            //    ccVal = null;
            //    return IOStatus.OK;
            //}
            //var lv = TargetDataset.EvaluateControlCodeString( cc );
            //if( lv < 0 ) {
            //    ccVal = null;
            //    return IOStatus.ValidationError;
            //}
            //ccVal = cc;
            //return IOStatus.OK;
        }

        public Code GetSWDimensionMember(string dim, string member)
        {
            EnsureMemberLookupInitialized();
            Code res;
            var key = BuildKey(dim, member);
            if (_DimensionMemberLookup.TryGetValue(key, out res)
                || _DimensionMemberLookup2.TryGetValue(key, out res)
                || _DimensionMemberLookup3.TryGetValue(key, out res))
            {
                return res;
            }

            key = BuildKey(dim, Code.SanitizeMemberCode(member));
            if (_DimensionMemberLookup.TryGetValue(key, out res)
                || _DimensionMemberLookup2.TryGetValue(key, out res)
                || _DimensionMemberLookup3.TryGetValue(key, out res))
            {
                return res;
            }
            return null;
        }

        public override bool Equals(object obj)
        {
            var oth = obj as SWMapper;
            if (oth == null || !base.Equals(oth))
            {
                return false;
            }

            // ReSharper disable once CompareOfFloatsByEqualityOperator
            return TargetDataset == oth.TargetDataset && PowerFactor == oth.PowerFactor &&
                   DecimalPlaces == oth.DecimalPlaces
                   && RetainTextFormat == oth.RetainTextFormat && ValueFieldName.IsSameAs(oth.ValueFieldName)
                   && string.Equals(_NumberFormat, oth._NumberFormat, StringComparison.Ordinal)
                   && SourceDataset.IsSameAs(oth.SourceDataset);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                int res = 7;
                res = (res + TargetDataset.GetHashCode())*31;
                res = (res + PowerFactor.GetHashCode())*31;
                res = (res + DecimalPlaces.GetHashCode())*31;
                res = (res + ValueFieldName.GetHashCode())*31;
                res = (res + (_NumberFormat ?? "").GetHashCode())*31;
                res = (res + (SourceDataset ?? "").ToLowerInvariant().GetHashCode())*31;
                return res;
            }
        }

        public char QualitativeSeparator { get; set; }
    }
}