﻿using System;

namespace DotStat.Transfer.Excel
{
    [Flags]
    public enum IOStatus
    {
        OK = 0,
        ExistingCoord = 1,
        ExistingProdValue = 2,
        DuplicateCoordError = 4,
        TargetCoordDynamicError = 8,
        ValidationError = 16,
        MissiongMemberError = 32,
        UnrecognizedMemberError = 64,
        FormatError = 128,
        GenericReadError = 256,
        GenericWriteError = 512,
        EOF = 1024,
        UnknownError = 2048,
        UnresolvedMemberError = MissiongMemberError | UnrecognizedMemberError,

        ReadError =
            DuplicateCoordError | UnknownError | GenericReadError | FormatError | UnresolvedMemberError |
            ValidationError,
        WriteError = DuplicateCoordError | UnknownError | GenericWriteError | ValidationError | TargetCoordDynamicError,
        IOError = ReadError | WriteError
    }
}