﻿using System;

namespace DotStat.Transfer.Excel.Util
{
    public interface IFastIndexer<T> : IIndexer<T>
    {
        bool TryGetValue(string idxName, IComparable key, out T res);

        void AddIndex(string idxName, Func<T, IComparable> keyGetter);
        void ResetIndices();
        //void ReLoad( IEnumerable<T> coll );
    }
}