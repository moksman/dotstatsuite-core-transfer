﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security;
using System.Xml.Linq;
using DotStat.Transfer.Excel.Exceptions;

namespace DotStat.Transfer.Excel.Util
{
    public static class Extensions
    {
        #region String

        public static bool IsSameAs(this string first, string second)
        {
            return string.Equals(first, second, StringComparison.InvariantCultureIgnoreCase);
        }

        public static string F(this string format, params object[] args)
        {
            if (args == null || args.Length == 0)
            {
                return format;
            }
            return string.Format(format, args);
        }

        public static FileInfo GetFileInfo(this string path)
        {
            try
            {
                return new FileInfo(path);
            }
            catch (SecurityException ex)
            {
                throw new UnrecoverableAppException("Security problem. Cannot access file: " + path, ex);
            }
            catch (PathTooLongException ex)
            {
                throw new UnrecoverableAppException("Path too long: " + path, ex);
            }
            catch (NotSupportedException ex)
            {
                throw new UnrecoverableAppException("Given file path format not supported: " + path, ex);
            }
            catch (UnauthorizedAccessException ex)
            {
                throw new UnrecoverableAppException("File access authorization problem: " + path, ex);
            }
        }

        #endregion

        #region XDocument

        public static XElement AddError(this XElement element, System.Exception ex)
        {
            var err = new XElement("Exception");
            AddExceptionDetails(err, ex);
            element.Add(err);
            return err;
        }

        private static void AddExceptionDetails(XElement element, System.Exception ex)
        {
            element.Add(new XElement("Message", ex.Message), new XElement("StackTrace", ex.StackTrace));
            if (ex.InnerException != null)
            {
                var inn = new XElement("InnerException");
                AddExceptionDetails(inn, ex.InnerException);
                element.Add(inn);
            }
            var re = ex as ReflectionTypeLoadException;
            if (re != null)
            {
                var tle = new XElement("LoaderException");
                foreach (var tlex in re.LoaderExceptions)
                {
                    AddExceptionDetails(tle, tlex);
                }
                element.Add(tle);
            }
        }

        #endregion

        #region Exceptions

        public static string GetUserMessage(this System.Exception ex, string customMessage = null)
        {
            if (ex is SWApplicationException || ex is UnrecoverableAppException)
            {
                return ex.Message;
            }
            return "An unexpected error has occured {0}: ".F(customMessage) + ex.Message;
        }

        #endregion

        #region XDocument

        public static void RemoveNamespaces(this XElement xElement)
        {
            xElement.Name = xElement.Name.LocalName;
            // ReSharper disable once CoVariantArrayConversion
            xElement.ReplaceAttributes(xElement.Attributes()
                .Where(
                    a =>
                        !a.IsNamespaceDeclaration && a.Name.NamespaceName != "http://www.w3.org/2001/XMLSchema-instance" &&
                        a.Name.LocalName != "xmlns")
                .Select(a => new XAttribute(a.Name.LocalName, a.Value))
                .ToArray());

            foreach (var xsub in xElement.Elements())
            {
                RemoveNamespaces(xsub);
            }
        }

        #endregion
    }
}