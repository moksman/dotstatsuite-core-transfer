FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
WORKDIR /app

ARG NUGET_FEED=https://api.nuget.org/v3/index.json

# copy csproj and restore as distinct layers
COPY DotStat.Transfer/*.csproj ./DotStat.Transfer/
COPY DotStat.Transfer.Excel/*.csproj ./DotStat.Transfer.Excel/
COPY DotStatServices.Transfer/*.csproj ./DotStatServices.Transfer/
COPY version.json Directory.Build.props ./

# restore nuget packages
RUN dotnet restore DotStatServices.Transfer -nowarn:NU1605 --source $NUGET_FEED -r linux-x64

# copy everything else
COPY . /app

RUN dotnet publish DotStatServices.Transfer -nowarn:NU1605 -c Release --no-restore -r linux-x64 -o /out

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2 AS runtime
WORKDIR /app
COPY --from=build /out .

ENTRYPOINT ["dotnet", "DotStatServices.Transfer.dll"]