# HISTORY

##  v4.0.3 2020-01-29 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with changes to the authentication management.

- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66
- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/52


##  v3.0.1 2020-01-22 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with a new entry in the dataspaces.private.json and the introduction of localization.json via the Dotstat.config nuget package.     

- sis-cc/.stat-suite/dotstatsuite-core-common/issues/102