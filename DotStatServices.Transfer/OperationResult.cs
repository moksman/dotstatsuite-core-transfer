﻿using System.Diagnostics.CodeAnalysis;

namespace DotStatServices.Transfer
{

    [ExcludeFromCodeCoverage]
    public class OperationResult
    {
        public static OperationResult OK = new OperationResult();

        public static OperationResult Error(string error)
        {
            return new OperationResult(false, error);
        }

        public bool Success { get; }
        public string Message { get; set; }
        public string Detail { get; set; }

        public OperationResult(bool success = true, string message = null)
        {
            Success = success;
            Message = message;
        }
    }
}