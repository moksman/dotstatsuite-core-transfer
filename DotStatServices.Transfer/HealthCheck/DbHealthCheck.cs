﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration.Interfaces;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DotStatServices.Transfer.HealthCheck
{
    /// <summary>
    /// 
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class DbHealthCheck : IHealthCheck
    {
        private readonly IDataspaceConfiguration _configuration;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configuration"></param>
        public DbHealthCheck(IDataspaceConfiguration configuration)
        {
            _configuration = configuration;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new CancellationToken())
        {
            var result = true;
            var data = new Dictionary<string, object>();

            foreach (var space in _configuration.SpacesInternal)
            {
                var structureAlive = await CheckSqlServerConnection(space.DotStatSuiteCoreStructDbConnectionString, cancellationToken);
                var dataAlive = await CheckSqlServerConnection(space.DotStatSuiteCoreDataDbConnectionString, cancellationToken);

                data.Add(space.Id, new
                {
                    structureAlive,
                    dataAlive
                });

                result &= structureAlive & dataAlive;
            }

            return result 
                ? HealthCheckResult.Healthy(data:data) 
                : HealthCheckResult.Unhealthy(data:data);
        }

        private async Task<bool> CheckSqlServerConnection(string connectionString, CancellationToken cancellationToken)
        {
            try
            {
                using (var connection = new SqlConnection(connectionString))
                {
                    await connection.OpenAsync(cancellationToken);

                    using (var command = connection.CreateCommand())
                    {
                        command.CommandText = "select 1";
                        await command.ExecuteScalarAsync(cancellationToken);
                    }
                }

                return true;
            }
            catch
            {
                return false;
            }
        }
    }
}
