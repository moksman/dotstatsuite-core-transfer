﻿using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using System.Collections.Generic;
using System.Net.Http;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Util.Io;

namespace DotStat.Transfer.Producer
{
    public class UrlProducer : IProducer<UrlToSqlTransferParam>
    {
        private readonly ReadableDataLocationFactory _dataLocationFactory;
        private readonly IMappingStoreDataAccess _dataAccess;

        public UrlProducer(IMappingStoreDataAccess dataAccess)
        {
            _dataAccess = dataAccess;
            _dataLocationFactory = new ReadableDataLocationFactory();
        }

        public bool IsAuthorized(UrlToSqlTransferParam transferParam, Dataflow dataflow)
        {
            return true;
        }

        public Dataflow GetDataflow(UrlToSqlTransferParam transferParam)
        {
            return _dataAccess.GetDataflow(transferParam.DestinationDataspace.Id,
                transferParam.SourceDataflow.AgencyId,
                transferParam.SourceDataflow.Id,
                transferParam.SourceDataflow.Version
            );
        }

        public TransferContent Process(UrlToSqlTransferParam transferParam, Dataflow dataflow)
        {
            var keyables = new List<IKeyable>();
            var datasetAttributes = new List<IKeyValue>();
            var content = new TransferContent()
            {
                Keyables = keyables,
                DatasetAttributes = datasetAttributes
            };

            var url = transferParam.Url == null
                ? $@"{transferParam.SourceDataspace.Endpointread}/data/{dataflow.Dsd.AgencyId},{dataflow.Dsd.Code},{dataflow.Dsd.Version}/all/?{transferParam.SourceQuery}"
                : transferParam.Url.ToString();

            // TODO Internal dataspace
            content.Observations =GetObservations(url, dataflow.Dsd, keyables, datasetAttributes);
               
            return content;
        }
        
        private IEnumerable<IObservation> GetObservations(string url, Dsd dsd, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            using (var client = new HttpClient())
            {
                var task = client.GetStreamAsync(url);
                var waiter = task.GetAwaiter();
                var manager = new DataReaderManager();
                    
                using (var sourceData = _dataLocationFactory.GetReadableDataLocation(waiter.GetResult()))
                {
                    using (var dataReaderEngine = manager.GetDataReaderEngine(sourceData, dsd.Base, null))
                    {
                        foreach (var observation in ReadSource(dataReaderEngine, keyables, datasetAttributes))
                        {
                            yield return observation;
                        }
                    }
                }
            }
        }

        private IEnumerable<IObservation> ReadSource(IDataReaderEngine reader, List<IKeyable> keyables, List<IKeyValue> datasetAttributes)
        {
            keyables.Clear();
            datasetAttributes.Clear();

            if (reader.MoveNextDataset())
            {
                datasetAttributes.AddRange(reader.DatasetAttributes);

                while (reader.MoveNextKeyable())
                {
                    var currentKeyable = reader.CurrentKey;

                    keyables.Add(currentKeyable);

                    if (reader.CurrentKey.Series && reader.MoveNextObservation())
                    {
                        do
                        {
                            var currentObservation = reader.CurrentObservation;

                            yield return currentObservation;
                        }
                        while (reader.MoveNextObservation());
                    }
                }
            }
        }

        public void Dispose()
        {
            //Nothing to do, IDataReaderEngine is within a "using" clause
        }
    }
}