﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Auth;
using DotStat.Common.Localization;
using DotStat.Db.Repository;
using DotStat.Domain;
using DotStat.Common.Logger;
using DotStat.MappingStore;
using DotStat.Transfer.DataflowManager;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;

namespace DotStat.Transfer.Producer
{
    ///TODO: Once used, include in unit tests
    [ExcludeFromCodeCoverage]
    public class SqlProducer<T> : IProducer<T> where T: TransferParam, ISqlTransferParam
    {
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IObservationRepository _observationRepository;
        private readonly IAttributeRepository _attributeRepository;
        private readonly IAuthorizationManagement _authorizationManagement;

        public SqlProducer(IMappingStoreDataAccess mappingStoreDataAccess, IObservationRepository observationRepository, IAttributeRepository attributeRepository, IAuthorizationManagement authorizationManagement)
        {
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _observationRepository = observationRepository;
            _attributeRepository = attributeRepository;
            _authorizationManagement = authorizationManagement;
        }

        public Dataflow GetDataflow(T transferParam)
        {
            var dataflow = _mappingStoreDataAccess.GetDataflow(
                transferParam.SourceDataspace.Id,
                transferParam.SourceDataflow.AgencyId,
                transferParam.SourceDataflow.Id,
                transferParam.SourceDataflow.Version);

            if (transferParam.DestinationDataspace != null)
            {
                Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataflowLoaded,
                            transferParam.CultureInfo.TwoLetterISOLanguageName),
                        dataflow.FullId,
                        transferParam.SourceDataspace.Id));
            }

            return dataflow;
        }

        public TransferContent Process(T transferParam, Dataflow dataflow)
        {
            if (!IsAuthorized(transferParam, dataflow))
            {
                throw new TransferUnauthorizedException();
            }

            var dataQuery = string.IsNullOrEmpty(transferParam.SourceQuery)
                ? new DataQueryImpl(
                    dataStructure: dataflow.Dsd.Base,
                    lastUpdated: null,
                    dataQueryDetail: null,
                    firstNObs: null,
                    lastNObs: null,
                    dataProviders: null,
                    dataflow: dataflow.Base,
                    dimensionAtObservation: null,
                    selectionGroup: null)
                : new DataQueryImpl(
                    new RESTDataQueryCore(string.Format("data/{0},{1},{2}/{3}",
                        transferParam.SourceDataflow.AgencyId,
                        transferParam.SourceDataflow.Id,
                        transferParam.SourceDataflow.Version,
                        transferParam.SourceQuery)),
                    new InMemoryRetrievalManager(new SdmxObjectsImpl(dataflow.Dsd.Base, dataflow.Base)));

            var content = new TransferContent();
            
            content.Observations = _observationRepository.GetObservations(dataQuery, transferParam.SourceDataspace.Id, transferParam.TargetVersion);

            content.Keyables = _attributeRepository.GetKeyables(dataQuery, transferParam.SourceDataspace.Id, transferParam.TargetVersion);

            content.DatasetAttributes = _attributeRepository.GetDatasetAttributes(dataQuery, transferParam.SourceDataspace.Id, transferParam.TargetVersion);

            return content;
        }

        public bool IsAuthorized(T transferParam, Dataflow dataflow)
        {
            return _authorizationManagement.IsAuthorized(
                transferParam.Principal, 
                transferParam.SourceDataspace.Id, 
                dataflow.AgencyId, 
                dataflow.Base.Id, 
                dataflow.Version.ToString(), 
                PermissionType.CanReadData
            );
        }

        public void Dispose()
        {
            //Nothing to do, sql connection  is within a "using" clause
        }
    }
}