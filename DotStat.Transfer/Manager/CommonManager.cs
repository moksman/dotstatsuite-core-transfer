﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Newtonsoft.Json.Linq;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;

namespace DotStat.Transfer.Manager
{
    public class CommonManager
    {
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IGeneralConfiguration _generalConfiguration;

        public CommonManager(IMappingStoreDataAccess mappingStoreDataAccess, IAuthorizationManagement authorizationManagement, IGeneralConfiguration generalConfiguration)
        {
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _authorizationManagement = authorizationManagement;
            _generalConfiguration = generalConfiguration;
        }

        public JObject GetPITInfo(ITransferParam transferParam, IDataflowMutableObject sourceDataFlow, ITransactionRepository transactionRepository)
        {
            var dataflow = GetDataflow(transferParam.SourceDataspace.Id, sourceDataFlow);

            if (!IsAuthorized(transferParam, dataflow))
            {
                throw new TransferUnauthorizedException();
            }
            
            return transactionRepository.GetDSDPITInfo(dataflow);
        }

        public void Rollback(ITransferParam transferParam, IDataflowMutableObject sourceDataFlow, IManagementRepository managementRepository, ITransactionRepository transactionRepository)
        {
            var dataflow = GetDataflow(transferParam.SourceDataspace.Id, sourceDataFlow);

            if (!IsAuthorized(transferParam, dataflow))
                throw new TransferUnauthorizedException();
            
            try
            {
                if (!transactionRepository.TryNewTransaction(transferParam.Id, dataflow, TargetVersion.PointInTime, true))
                    throw new TransferFailedException();

                transactionRepository.Rollback(dataflow, _mappingStoreDataAccess);
                managementRepository.DeleteTransactionItem(transferParam.Id, true);
            }
            catch (System.Exception)
            {
                managementRepository.DeleteTransactionItem(transferParam.Id, false);
                throw;
            }
        }

        public void Restore(TransferParam transferParam, IDataflowMutableObject sourceDataFlow, IManagementRepository managementRepository, ITransactionRepository transactionRepository)
        {
            var dataflow = GetDataflow(transferParam.SourceDataspace.Id, sourceDataFlow);
            if (!IsAuthorized(transferParam, dataflow))
                throw new TransferUnauthorizedException();

            try
            {
                if (!transactionRepository.TryNewTransaction(transferParam.Id, dataflow, TargetVersion.Live, true))
                    throw new TransferFailedException();

                transactionRepository.Restore(dataflow, _mappingStoreDataAccess);
                managementRepository.DeleteTransactionItem(transferParam.Id, true);                
            }
            catch (System.Exception)
            {
                managementRepository.DeleteTransactionItem(transferParam.Id, false);
                throw;
            }
        }

        private bool IsAuthorized(ITransferParam transferParam, Dataflow dataflow)
        {
            return _authorizationManagement.IsAuthorized(
                transferParam.Principal,
                transferParam.SourceDataspace.Id,
                dataflow.AgencyId,
                dataflow.Base.Id,
                dataflow.Version.ToString(),
                PermissionType.CanImportData
            );
        }

        private Dataflow GetDataflow(string sourceDataSpaceId, IDataflowMutableObject sourceDataflow)
        {
            var dataflow = _mappingStoreDataAccess.GetDataflow(
                sourceDataSpaceId,
                sourceDataflow.AgencyId,
                sourceDataflow.Id,
                sourceDataflow.Version);

            Log.Notice(
                string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataflowLoaded),
                    dataflow.FullId,
                    sourceDataSpaceId));

            return dataflow;
        }
    }
}
