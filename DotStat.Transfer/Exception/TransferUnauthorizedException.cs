﻿
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Exception
{
    public class TransferUnauthorizedException : System.Exception
    {
        [ExcludeFromCodeCoverage]
        public TransferUnauthorizedException()
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferUnauthorizedException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferUnauthorizedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
