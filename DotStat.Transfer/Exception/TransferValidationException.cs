﻿
using System.Diagnostics.CodeAnalysis;

namespace DotStat.Transfer.Exception
{
    public class TransferValidationException : System.Exception
    {
        [ExcludeFromCodeCoverage]
        public TransferValidationException()
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferValidationException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public TransferValidationException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
