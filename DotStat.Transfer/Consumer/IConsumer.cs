﻿using DotStat.Domain;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Consumer
{
    public interface IConsumer<in T> where T : ITransferParam
    {
        bool Save(T transferParam, Dataflow dataflow, TransferContent transferContent);

        bool IsAuthorized(ITransferParam transferParam, Dataflow dataflow);
    }
}