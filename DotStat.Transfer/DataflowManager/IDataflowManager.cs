﻿
using DotStat.Domain;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.DataflowManager
{
    public interface IDataflowManager<in T> where T : ITransferParam
    {
        Dataflow GetDataflow(T transferParam);
    }
}
