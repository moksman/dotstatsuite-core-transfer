﻿using System;
using DotStat.Common.Configuration.Dto;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;

namespace DotStat.Transfer.Param
{
    public interface IUrlToSqlTransferParam
    {
        IDataflowMutableObject SourceDataflow { get;}
        string SourceQuery { get; }
        DataspaceExternal SourceDataspace { get; set; }
    }


    public class UrlToSqlTransferParam : TransferParam, IUrlToSqlTransferParam, ISqlTransferParam
    {
        public IDataflowMutableObject SourceDataflow { get; set; }
        public IDataflowMutableObject DestinationDataflow { get; set; }
        public string SourceQuery { get; set; }
        public new DataspaceExternal SourceDataspace { get; set; }
        public Uri Url { get; set; }

    }
}
